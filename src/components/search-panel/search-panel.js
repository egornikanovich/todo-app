import React, { Component } from 'react';
import'./search-panel.css';

export default class SearchPanel extends Component {
    onSearchfieldChange = (e) => {
        const { onSearch } = this.props;
        onSearch(e.target.value);
    }

    render () {
        const { onFilter } = this.props;
        const { filters } = this.props;
        let classesListBtnImportant = 'btn btn-secondary';
        let classesListBtnDone = 'btn btn-secondary';
        let classesListBtnActive = 'btn btn-secondary';
        if(filters.important) {
            classesListBtnImportant += ' active'
        }
        if(filters.done) {
            classesListBtnDone += ' active'
        }
        if (!filters.done && !filters.important) {
            classesListBtnActive += ' active'
        }
        return (
            <div className="d-flex">
                <input 
                    onChange = { this.onSearchfieldChange }
                    placeholder="search" className="search-panel"/>
                <div className="btn-group btn-group-toggle" data-toggle="buttons">
                    <label className={ classesListBtnActive }>
                    <input 
                        onClick = { () => onFilter(false, false) }
                        type="radio" name="options" id="option1" autoComplete="off" /> All
                    </label>
                    <label className={ classesListBtnImportant }>
                    <input 
                        onClick = { () => onFilter(true, false)  }
                        type="radio" name="options" id="option2" autoComplete="off"/> Important
                    </label>
                    <label className={ classesListBtnDone }>
                    <input 
                        onClick = { () => onFilter(false, true)  }
                        type="radio" name="options" id="option3" autoComplete="off"/> Done
                    </label>
                </div>
            </div>
        );
    }
}

