import React, { Component } from 'react';
import './todo-list-item.css';

export default class TodoListItem extends Component {
    render() {
        const { label, 
                onDeleted, 
                onToggleImportant, 
                onToggleDone, 
                important, 
                done
                } = this.props; 
                
        let itemClassNames = 'todo-list-item';
        if(done) {
            itemClassNames += ' done';
        }
        if(important) {
            itemClassNames += ' important';
        }
    
        return (
        <div>
            <span onClick ={ onToggleDone } className={ itemClassNames }>{ label } </span>
        <span>
            <button onClick={ onDeleted } className="btn btn-remove"><i className="fa fa-minus-square-o" aria-hidden="true"></i></button>
            <button onClick={ onToggleImportant } className="btn btn-important"><i className="fa fa-exclamation" aria-hidden="true"></i></button>
        </span>
        </div>);
    };
}