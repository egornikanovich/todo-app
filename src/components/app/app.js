import React, { Component } from 'react';

import AppHeader from '../app-header/';
import SearchPanel from '../search-panel/';
import TodoList from '../todo-list/';
import AddTodoItemForm from '../add-todo-item-form/';


export default class App extends Component {
    constructor () {
        super();
        this.newID = 1;
        this.state = {
            todoData: [
                this.createTodoItem('Learn JS'),
                this.createTodoItem('Drink some tea'),
                this.createTodoItem('Make a dinner'),
                this.createTodoItem('Cardio')
        ],
            searchQuery: '',
            filter: {
                important: false,
                done: false
            }
        }
    }

    createTodoItem(label) {
        return {
            label,
            important: false,
            done: false,
            id: this.newID++,
        }
    }

    deleteTask = (id) => {
        this.setState (({ todoData }) => {
            const index = todoData.findIndex((el => el.id === id))
            const todoDataUpdated = [
                ...todoData.slice(0, index), 
                ...todoData.slice(index+1)]
            return {
                todoData: todoDataUpdated
            }
        })
    }

    addTask = (text) => {
        if(text !== ''){
            this.setState (({todoData}) => {
            const newItem = this.createTodoItem(text);
            const todoDataUpdated = [...todoData.slice(0), newItem]
                return {
                todoData: todoDataUpdated
                }
            });
        } else {
        alert('Name your task!~');
        }
    }

    searchTask(items, query) {
        if(query.length === 0) {
            return items;
        }
        return items.filter((item)=>{
            return item.label
            .toLowerCase()
            .indexOf(query.toLowerCase()) > -1;
        });
    }

    getSearchQuery = (query) => {
        this.setState({
            searchQuery: query
        })
    }

    toggleProperty(array, id, propName) {
            const index = array.findIndex((el => el.id === id))
            const oldItem = array[index];
            const newItem = {...oldItem, 
                            [propName] : !oldItem[propName]};
            return [...array.slice(0, index), 
                    newItem,
                    ...array.slice(index+1)]
    }

    onToggleImportant = (id) => {
        this.setState (({ todoData }) => {
            return {
                todoData:   this.toggleProperty(todoData, id, 'important')
            }
        })
    }

    onToggleDone = (id) => {
        this.setState (({ todoData }) => {
            return {
                todoData:   this.toggleProperty(todoData, id, 'done')
            }
        })
    }

    filterImportant(array) {
        return array.filter(el => el.important)
    }

    filterDone(array) {
        return array.filter(el => el.done)
    }
    
    setFilter = (bool, bool2) => {
        if(bool) {
        this.setState({
            filter: {
                important: true,
                done: false
                }
            })
        }
        if(bool2) {
            this.setState({
                filter: {
                    important: false,
                    done: true
                    }
                })
        } 
        if(!bool & !bool2) {
            this.setState({
                filter: {
                    important: false,
                    done: false
                    }
                })
        }

    } 
    render() {
  
        const { todoData, searchQuery, filter } = this.state;
        let visibleItems = this.searchTask(todoData, searchQuery);
        if (this.state.filter.important) {
            visibleItems = this.filterImportant(visibleItems);
        } 
        if (this.state.filter.done) {
            visibleItems = this.filterDone(visibleItems);
        }
        const doneCount = todoData.filter((el) => el.done).length;
        const todoCount = todoData.length - doneCount;

    return (
        <div>
            <AppHeader 
                todoNumber = { todoCount } 
                doneNumber ={ doneCount } 
            />
            <SearchPanel 
                onSearch = { this.getSearchQuery }
                onFilter = { this.setFilter }
                filters = { filter }
            />
            <TodoList 
                todos = { visibleItems } 
                onDeleted = { this.deleteTask }
                onToggleImportant = { this.onToggleImportant }
                onToggleDone = { this.onToggleDone}
            />
            <AddTodoItemForm 
                onAdded = { this.addTask }
            />
        </div>
    );}
};

