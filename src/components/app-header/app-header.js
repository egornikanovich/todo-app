import React from 'react';
import './app-header.css';

const AppHeader = ({todoNumber, doneNumber}) => {
    return (
    <div className="app-header-wrapper">
        <h1 className="app-header">My Todo List</h1>
        <h2 className="app-subheader">{ todoNumber } tasks are active, { doneNumber } are done.</h2>
    </div>);
};

export default AppHeader;