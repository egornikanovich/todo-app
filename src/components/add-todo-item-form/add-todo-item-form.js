import React, { Component } from 'react';
import './add-todo-item-form.css'

export default class AddTodoItemForm extends Component {
    constructor() {
        super();
        this.state = {
            label: ''
        }
    }
    onLabelChange = (e) => {
        this.setState({
            label: e.target.value
        });
    };

    onSubmit = (e)  => {
        e.preventDefault();
        this.props.onAdded(this.state.label);
        this.setState({
            label: ''
        });
    }

    render () {
        return(
            <form className="d-flex input-form"
                  onSubmit={ this.onSubmit }>
            <input placeholder="What needs to be done?" 
                   type="text" 
                   className="add-item-form"
                   onChange={ this.onLabelChange }
                   value={ this.state.label }
            />
            <button className='btn'>Add item</button>
            </form>
        );
    }
}